﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterController : MonoBehaviour {

	public const float power = 1.0f;
	public const float turning_force = 150;

	private CharacterController cc;
	private Rigidbody rb;

	public int Player;
	public Camera PlayerCam;

	private float speed;
	private Vector3 direction;

	private Dictionary<int, float> dragLookup = new Dictionary<int, float>();
	private Dictionary<int, float> maxLookup = new Dictionary<int, float>{
		{0, 50 * CharacterController.power},
		{1, 100 * CharacterController.power},
		{2, 5 * CharacterController.power}
	};

	private Dictionary<int, float> accelerationLookup = new Dictionary<int, float>{
		{0, 2 * CharacterController.power},
		{1, 3 * CharacterController.power},
		{2, CharacterController.power}
	};

	// Use this for initialization
	void Start () {

		foreach (var entry in maxLookup) {
			dragLookup[entry.Key] = this.calculateDragForMaxSpeed(entry.Value, this.accelerationLookup[entry.Key]);
		}

		this.direction = this.gameObject.transform.forward;
	}

	// Update is called once per frame
	void Update () {
		int texture = TerrainSurface.GetMainTexture (this.gameObject.transform.position);

		float accel = this.accelerationLookup [texture];
		float drag = this.dragLookup [texture];
		float speed = this.speed;

		speed += Input.GetAxis("Player" + this.Player + " Vert") * accel;
		speed *= (1 - drag);

		if (Mathf.Abs(speed) < 1) {
			speed = 0;
		}

		this.speed = speed;

		this.gameObject.transform.position += this.direction * speed * Time.deltaTime;
		this.gameObject.transform.Rotate (0, Input.GetAxis("Player" + this.Player + " Horiz") * Time.deltaTime * CharacterController.turning_force, 0);

		this.direction = 0.95f * this.direction + 0.05f * this.gameObject.transform.forward;
		this.direction.Normalize();

		float cam_dist = (Mathf.Min(100f, this.speed) / 100f);

		this.PlayerCam.transform.localPosition = new Vector3(0f, 2f + cam_dist * 4f, -5f + cam_dist * -10f);
	}

	float calculateDragForMaxSpeed(float maxspeed, float acceleration) {
		return acceleration / (maxspeed + acceleration);
	}
}