﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	public Vector3 currPos;
	public Vector3 prevPos;

	public float aggregateAngle;

	void Start () {
		currPos = this.gameObject.transform.position;
		prevPos = currPos;

		aggregateAngle = 0.0f;
	}

	void Update () {
		
	}
}
