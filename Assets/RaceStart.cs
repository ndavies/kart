﻿using UnityEngine;
using System.Collections;

public class RaceStart : MonoBehaviour {


	public GameObject[] racers;
	public GameObject center;

	public GameObject finishLine;

	private const int sectionRes = 8;

	void Start () {

		// look at the start - 0 degrees
		center.transform.LookAt (finishLine.transform.position);

		// TODO: work out all aggregate angles


	}

	void Update () {

		Vector3 vCenter = center.transform.position;
		
		Debug.Log (racers[0].GetComponent<Player>().aggregateAngle);

		// tell each player their position + quadrant
		foreach (GameObject g in racers) {
			Player p = g.GetComponent<Player>();
		
			p.prevPos = p.currPos;
			p.currPos = g.transform.position;

			p.prevPos.y = 0;
			p.currPos.y = 0;

			Vector3 prevDiff = p.prevPos - vCenter;
			Vector3 currDiff = p.currPos - vCenter;

			float angleBetween = Vector3.Angle(prevDiff, currDiff);
			Debug.Log (angleBetween);

			prevDiff.y = 0;
			prevDiff.Normalize();

			Vector3 rightPrev = new Vector3(prevDiff.z, 0, -prevDiff.x);

			currDiff.Normalize();

			float dotResult = Vector3.Dot (rightPrev, currDiff);

			if (dotResult < 0)
				dotResult = -1;
			else
				dotResult = 1;

			if (p.aggregateAngle < 360.0f) {
				if (p.aggregateAngle + angleBetween * dotResult >= 360.0f) {
					Debug.Log ("LAP!");
				}
			}

			p.aggregateAngle += angleBetween * dotResult;
		}
	}
}
